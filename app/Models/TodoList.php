<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TodoList extends Model
{
    protected $table = 'a2_todo_list';
    public $timestamps = false;
    protected $primaryKey = "Id";
}
