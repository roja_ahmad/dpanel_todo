<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserApps extends Model
{
    protected $table = 'a1_user_apps';
    public $timestamps = false;
}
