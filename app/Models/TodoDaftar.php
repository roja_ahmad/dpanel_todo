<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TodoDaftar extends Model
{
    protected $table = 'a2_todo_daftar';
    public $timestamps = false;
    protected $primaryKey = "Id";
}
