<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserApps;
use Image;
use Validator;
use Session;

class AppController extends Controller
{
    public function index()
    {
        $sesid = session()->get('id');
        $data = (object) array(
            "title" => "App Setting",
            "data_apps" => UserApps::where('id', 1)->first(),
            "data_user" => User::where('id', $sesid)->first(),
        );
        return view('management.index', compact('data'));
    }

    public function updateApps(Request $request, $id)
    {
        $validator = Validator::make(
            $request->all(),

            array(
                'nama_toko' => 'required'
            ),

            array(
                'nama_toko.required' => 'Nama toko tidak boleh kosong'
            )
        );

        if ($validator->fails()) {
            $eror = $validator->messages()->all();

            return redirect('/app_setting')->withInput($request->input())->withErrors($validator);
        } else {
            $nama_toko = $request->nama_toko;
            $alias_toko = $request->alias_toko;
            $alamat_toko = $request->alamat_toko;
            $no_telp_toko = $request->no_telp_toko;
            $email_toko = $request->email_toko;

            $data = UserApps::where('id', $id)->first();
            // $data = new UserApps(); // Untuk Tambah data
            $data->nama_apps = $nama_toko;
            $data->nama_lain = $alias_toko;
            $data->alamat = $alamat_toko;
            $data->no_telp = $no_telp_toko;
            $data->email = $email_toko;

            if ($request->file('image')) {
                if ($data->img_apps != "") {
                    $path = 'assets/img/logoweb/' . $data->img_apps;
                    unlink(public_path($path));
                }
                $randomimg_apps = rand(10, 100);
                $img_apps = $request->file('image');
                $input['imagename'] = $randomimg_apps . date('ymdhis') . '.' . $img_apps->getClientOriginalExtension();
                $destinationPath = ('assets/img/logoweb');

                $img_apps->move(public_path($destinationPath), $input['imagename']);

                $direktori = $input['imagename'];

                $data->img_apps = $direktori;
            }

            if ($request->file('image2')) {
                if ($data->logo_apps != "") {
                    $path = 'assets/img/logoweb/' . $data->logo_apps;
                    unlink(public_path($path));
                }
                $randomlogo_apps = rand(10, 100);
                $logo_apps = $request->file('image2');
                $input['imagename'] = $randomlogo_apps .  date('ymdhis') . '.' . $logo_apps->getClientOriginalExtension();
                $destinationPath = ('assets/img/logoweb');

                $logo_apps->move(public_path($destinationPath), $input['imagename']);

                $direktori = $input['imagename'];

                $data->logo_apps = $direktori;
            }

            $data->save();
            return redirect('/app_setting')->with('success', 'Berhasil ubah App <strong>' . $data->nama_apps . '</strong>');
        }
    }
}
