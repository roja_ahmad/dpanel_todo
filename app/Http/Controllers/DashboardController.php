<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserApps;
use App\Models\TodoDaftar;
use App\Models\TodoList;
use Validator;

class DashboardController extends Controller
{
    public function index()
    {
        $sesid = session()->get('id');
        $data = (object) array(
            "title" => "Dashboard To Do List",
            "data_apps" => UserApps::where('id', 1)->first(),
            "data_user" => User::where('id', $sesid)->first(),
            "list_daftar" => TodoDaftar::get(),
            "tot1" => TodoList::where('tanggal', date('Y-m-d'))->count(),
            "tot2" => TodoList::where('perioritas', '1')->count(),
            "tot3" => TodoList::get()->count(),
            "tot4" => TodoList::where('flag', 'Y')->count(),
        );
        return view('pageview.index', compact('data'));
    }

    // TODO DAFTAR

    public function setup_daftar()
    {
        $sesid = session()->get('id');
        $data = (object) array(
            "title" => "Daftar To Do List",
            "data_apps" => UserApps::where('id', 1)->first(),
            "data_user" => User::where('id', $sesid)->first()
        );
        return view('pageview.add_daftar', compact('data'));
    }

    public function edit_daftar(Request $request, $id)
    {
        $sesid = session()->get('id');
        $data = (object) array(
            "title" => "Edit Daftar To Do List",
            "data_apps" => UserApps::where('id', 1)->first(),
            "data_user" => User::where('id', $sesid)->first(),
            "data_daftar" => TodoDaftar::where('id', $id)->first()
        );
        return view('pageview.edit_daftar', compact('data'));
    }

    public function save_daftar(Request $request)
    {
        $validator = Validator::make(
            $request->all(),

            array(
                'nama_daftar' => 'required'
            ),

            array(
                'nama_daftar.required' => 'Nama daftar tidak boleh kosong'
            )
        );

        if ($validator->fails()) {
            $eror = $validator->messages()->all();

            return redirect('/setup_daftar')->withInput($request->input())->withErrors($validator);
        } else {
            $nama_daftar = $request->nama_daftar;
            $icon_daftar = $request->icon_daftar;

            $data = new TodoDaftar(); // Untuk Tambah data
            $data->nama_daftar = $nama_daftar;
            $data->icon_daftar = $icon_daftar;

            $data->save();
            return redirect('/dashboard')->with('success', 'Berhasil perbarui daftar ' . $data->nama_daftar . '');
        }
    }

    public function save_edit_daftar(Request $request, $id)
    {
        $validator = Validator::make(
            $request->all(),

            array(
                'nama_daftar' => 'required'
            ),

            array(
                'nama_daftar.required' => 'Nama daftar tidak boleh kosong'
            )
        );

        if ($validator->fails()) {
            $eror = $validator->messages()->all();

            return redirect('/edit_daftar' . '/' . $id)->withInput($request->input())->withErrors($validator);
        } else {
            $nama_daftar = $request->nama_daftar;
            $icon_daftar = $request->icon_daftar;

            $data = TodoDaftar::where('Id', $id)->first();
            $data->nama_daftar = $nama_daftar;
            $data->icon_daftar = $icon_daftar;

            $data->save();
            return redirect('/dashboard')->with('success', 'Berhasil perbarui daftar ' . $data->nama_daftar . '');
        }
    }

    public function save_hapus_daftar(Request $request, $id)
    {
        $data = TodoDaftar::where('Id', $id)->delete();

        return redirect('/dashboard')->with('success', 'Berhasil hapus daftar');
    }

    // TODO PENGINGAT

    public function view_todo(Request $request, $code, $id)
    {
        $sesid = session()->get('id');
        $data = (object) array(
            "title" => "To Do List",
            "data_apps" => UserApps::where('id', 1)->first(),
            "data_user" => User::where('id', $sesid)->first(),
            "todo" => TodoList::where($code, $id)->get(),
        );
        return view('pageview.view_todo', compact('data'));
    }

    public function setup_todo()
    {
        $sesid = session()->get('id');
        $data = (object) array(
            "title" => "Tambah To Do List",
            "data_apps" => UserApps::where('id', 1)->first(),
            "data_user" => User::where('id', $sesid)->first(),
            "daftar" => TodoDaftar::get()
        );
        return view('pageview.add_todo', compact('data'));
    }

    public function edit_todo(Request $request, $id)
    {
        $sesid = session()->get('id');
        $data = (object) array(
            "title" => "Edit To Do List",
            "data_apps" => UserApps::where('id', 1)->first(),
            "data_user" => User::where('id', $sesid)->first(),
            "daftar" => TodoDaftar::get(),
            "todo" => TodoList::where('Id', $id)->first()
        );
        return view('pageview.edit_todo', compact('data'));
    }

    public function save_todo(Request $request)
    {
        $validator = Validator::make(
            $request->all(),

            array(
                'judul' => 'required'
            ),

            array(
                'judul.required' => 'Judul tidak boleh kosong'
            )
        );

        if ($validator->fails()) {
            $eror = $validator->messages()->all();

            return redirect('/setup_todo')->withInput($request->input())->withErrors($validator);
        } else {
            $judul = $request->judul;
            $catatan = $request->catatan;
            $daftar_id = $request->daftar_id;
            $tanggal = $request->tanggal;
            $flag = $request->flag;
            $prioritas = $request->prioritas;

            $data = new TodoList(); // Untuk Tambah data
            $data->judul = $judul;
            $data->catatan = $catatan;
            $data->daftar_id = $daftar_id;
            $data->tanggal = $tanggal;
            $data->flag = $flag;
            $data->perioritas = $prioritas;

            $data->save();
            return redirect('/dashboard')->with('success', 'Berhasil perbarui daftar ' . $data->judul . '');
        }
    }

    public function save_edit_todo(Request $request, $id)
    {
        $validator = Validator::make(
            $request->all(),

            array(
                'judul' => 'required'
            ),

            array(
                'judul.required' => 'Judul tidak boleh kosong'
            )
        );

        if ($validator->fails()) {
            $eror = $validator->messages()->all();

            return redirect('/setup_todo')->withInput($request->input())->withErrors($validator);
        } else {
            $judul = $request->judul;
            $catatan = $request->catatan;
            $daftar_id = $request->daftar_id;
            $tanggal = $request->tanggal;
            $flag = $request->flag;
            $prioritas = $request->prioritas;

            $data = TodoList::where('Id', $id)->first();
            $data->judul = $judul;
            $data->catatan = $catatan;
            $data->daftar_id = $daftar_id;
            $data->tanggal = $tanggal;
            $data->flag = $flag;
            $data->perioritas = $prioritas;

            $data->save();
            return redirect('/dashboard')->with('success', 'Berhasil perbarui daftar ' . $data->judul . '');
        }
    }

    public function save_hapus_todo(Request $request, $id)
    {
        $data = TodoList::where('Id', $id)->delete();

        return redirect('/dashboard')->with('success', 'Berhasil selesaikan todo');
    }
}
