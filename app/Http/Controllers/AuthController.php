<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Contracts\Session\Session as SessionSession;
use Validator;
use Session;
use App\Models\UserApps;

class AuthController extends Controller
{
    public function index()
    {
        if (session()->get('id') && session()->get('is_active') == '1') {
            return redirect('dashboard');
        } else {

            $data = (object) array(
                "title" => "Login",
                "data_apps" => UserApps::where('id', 1)->first(),
            );
            return view('auth.login', compact('data'));
        }
    }

    public function logout()
    {
        Session::flush();
        Session::save();

        return redirect('/login');
    }

    public function verify(Request $request)
    {
        $validator = Validator::make(
            $request->all(),

            array(
                'username' => 'required',
                'password' => 'required',
            ),

            array(
                'username.required' => 'username tidak boleh kosong',
                'password.required' => 'Password tidak boleh kosong',
            )
        );

        //Kondisi validasi (Pesan Error yang akan terjadi) 
        if ($validator->fails()) {

            $eror = $validator->messages()->all();

            return redirect('/login')->withInput($request->input())->withErrors($validator);
        } else {

            $username = $request->username;
            $password = $request->password;

            $admin = User::where('username', $username)->first();

            if ($admin) {
                if ($admin->is_active == 0) {
                    return redirect('/login')->with('fail', 'Akun Anda Tidak Aktif!');
                } else {


                    //Pengecekan Password 
                    $cekpass = User::where('username', $username)->where('password', md5($password))->first();
                    if ($cekpass) {
                        $ds = User::where('username', $username)->first();
                        Session::put('id', $ds->id);
                        Session::put('name', $ds->name);
                        Session::put('username', $ds->username);
                        Session::put('email', $ds->email);
                        Session::put('role_id', $ds->role_id);
                        Session::put('is_active', $ds->is_active);
                        Session::save();
                        return redirect('dashboard');
                    } else {
                        return redirect('/login')->with('fail', 'Password anda Salah!');
                    }
                }
            } else {
                return redirect('/login?error=1')->with('fail', 'Username Tidak Ditemukan!');
            }
        }
    }
}
