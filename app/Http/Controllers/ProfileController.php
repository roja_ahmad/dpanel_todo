<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserApps;

class ProfileController extends Controller
{
    public function index()
    {
        $sesid = session()->get('id');
        $data = (object) array(
            "title" => "Data Profile",
            "data_apps" => UserApps::where('id', 1)->first(),
            "data_user" => User::where('id', $sesid)->first(),
        );
        return view('preferensi.index', compact('data'));
    }
}
