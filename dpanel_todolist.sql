﻿# Host: localhost  (Version 5.5.5-10.4.24-MariaDB)
# Date: 2022-05-31 23:54:51
# Generator: MySQL-Front 6.0  (Build 2.20)


#
# Structure for table "a1_user"
#

DROP TABLE IF EXISTS `a1_user`;
CREATE TABLE `a1_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `username` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `image` varchar(128) DEFAULT NULL,
  `password` varchar(256) NOT NULL,
  `role_id` int(11) NOT NULL,
  `is_active` int(1) NOT NULL,
  `date_created` int(11) NOT NULL,
  `last_log` datetime NOT NULL,
  `unit` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

#
# Data for table "a1_user"
#

INSERT INTO `a1_user` VALUES (1,'Dev Admin','admin','ahmad@gmail.com','ava_male.png','81dc9bdb52d04dc20036dbd8313ed055',1,1,1571924033,'2022-05-29 15:52:46',0);

#
# Structure for table "a1_user_apps"
#

DROP TABLE IF EXISTS `a1_user_apps`;
CREATE TABLE `a1_user_apps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_apps` varchar(128) NOT NULL,
  `nama_lain` varchar(125) NOT NULL,
  `img_apps` varchar(128) NOT NULL DEFAULT '',
  `logo_apps` varchar(128) NOT NULL DEFAULT '',
  `no_telp` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `alamat` varchar(128) NOT NULL,
  `kecamatan` varchar(125) NOT NULL,
  `version` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

#
# Data for table "a1_user_apps"
#

INSERT INTO `a1_user_apps` VALUES (1,'TODO PANEL','TODO PANEL','72220530041126.png','28220530041126.png','085261033321','todo@todo.com','Jl. Pocut Baren No.49-57, Keuramat, Kuta Alam','Banda Aceh','v-1.0');

#
# Structure for table "a2_todo_daftar"
#

DROP TABLE IF EXISTS `a2_todo_daftar`;
CREATE TABLE `a2_todo_daftar` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_daftar` varchar(255) DEFAULT NULL,
  `icon_daftar` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

#
# Data for table "a2_todo_daftar"
#

INSERT INTO `a2_todo_daftar` VALUES (1,'Routine List','icon-list'),(8,'Pengingat','icon-fire'),(9,'Olahraga','icon-social-dribbble');

#
# Structure for table "a2_todo_list"
#

DROP TABLE IF EXISTS `a2_todo_list`;
CREATE TABLE `a2_todo_list` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(255) DEFAULT NULL,
  `catatan` varchar(255) DEFAULT NULL,
  `daftar_id` varchar(255) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `flag` enum('Y','N') DEFAULT 'N',
  `perioritas` enum('0','1') DEFAULT '0' COMMENT '0-n/a, 1-tinggi',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

#
# Data for table "a2_todo_list"
#

INSERT INTO `a2_todo_list` VALUES (4,'Main Bola','main bola nanti sore','9','2022-05-31','N','0'),(5,'Bayar Pajak','bayar pajak motor','8','2022-05-31','Y','1'),(7,'Bayar Uang Miko','bayar hutang dengan miko','8','2022-05-31','N','1'),(8,'Pergi Les','Les Piano','1','2022-05-31','N','0'),(9,'Belanja Bulanan','belanja susu sudah habis','1','2022-05-31','N','0'),(10,'Jalan dengan dia','janjian ngedate','1','2022-05-31','N','0');
