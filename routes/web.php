<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\AuthController::class, 'index']);
Route::get('/login', [App\Http\Controllers\AuthController::class, 'index']);
Route::post('/login', [App\Http\Controllers\AuthController::class, 'verify']);
Route::get('/logout', [App\Http\Controllers\AuthController::class, 'logout']);

Route::group(['middleware' => 'App\Http\Middleware\Auth'], function () {
    Route::get('/profile', [App\Http\Controllers\ProfileController::class, 'index']);
    Route::get('/app_setting', [App\Http\Controllers\AppController::class, 'index']);
    Route::post('/app_setting/{id}', [App\Http\Controllers\AppController::class, 'updateApps']);
    Route::post('/save_daftar', [App\Http\Controllers\DashboardController::class, 'save_daftar']);
    Route::post('/jsondatadaftar', [App\Http\Controllers\DashboardController::class, 'jsondatadaftar']);
    Route::get('/dashboard', [App\Http\Controllers\DashboardController::class, 'index']);
    Route::get('/setup_daftar', [App\Http\Controllers\DashboardController::class, 'setup_daftar']);
    Route::get('/view_todo/{code}/{id}', [App\Http\Controllers\DashboardController::class, 'view_todo']);
    Route::get('/edit_daftar/{id}', [App\Http\Controllers\DashboardController::class, 'edit_daftar']);
    Route::post('/save_edit_daftar/{id}', [App\Http\Controllers\DashboardController::class, 'save_edit_daftar']);
    Route::get('/save_hapus_daftar/{id}', [App\Http\Controllers\DashboardController::class, 'save_hapus_daftar']);
    Route::get('/setup_todo', [App\Http\Controllers\DashboardController::class, 'setup_todo']);
    Route::get('/edit_todo/{id}', [App\Http\Controllers\DashboardController::class, 'edit_todo']);
    Route::post('/save_todo', [App\Http\Controllers\DashboardController::class, 'save_todo']);
    Route::post('/save_edit_todo/{id}', [App\Http\Controllers\DashboardController::class, 'save_edit_todo']);
    Route::get('/save_hapus_todo/{id}', [App\Http\Controllers\DashboardController::class, 'save_hapus_todo']);
});
