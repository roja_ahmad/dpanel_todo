@extends('template.app')
@section('content')

<div class="page-title">
    <div class="container">
        <h3 class="text-center">{{ $data->title }}</h3>
    </div>
</div>
<div id="main-wrapper" class="container">

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-white">
                <div class="panel-body">
                    <form class="m-t-md" action="{{ url('save_daftar') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th width="5%">No</th>
                                    <th width="20%">Judul</th>
                                    <th>Catatan</th>
                                    <th width="15%" class="text-center"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                $no =1;
                                @endphp
                                @foreach ($data->todo as $ds)
                                <tr>
                                    <td class="text-center">{{$no++}}</td>
                                    <td>{{$ds->judul}}</td>
                                    <td>{{$ds->catatan}}</td>
                                    <td class="text-right">
                                        <a href="{{url('/edit_todo',$ds->Id)}}" class="btn btn-default btn-xs"
                                            title="Edit To DO"><i class="fa fa-pencil"></i></a>
                                        <a href="{{url('/save_hapus_todo',$ds->Id)}}"
                                            onclick="return confirm('Selesaikan tugas ini?')"
                                            class="btn btn-success btn-xs" title="Selesaikan"><i
                                                class="fa fa-check"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                        <div style="float:right;">
                            <a href="{{url('/')}}" class="btn btn-warning">Kembali</a>
                            {{-- <button type="submit" class="btn btn-success">
                                <i class="glyphicon glyphicon-ok m-r-xs"></i>Simpan Daftar
                            </button> --}}
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function getIcon(obj) {
        var getIcon = $(obj).attr('class')
        $("#icon_daftar").val(getIcon);
    }
</script>

@endsection