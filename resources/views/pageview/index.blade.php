@extends('template.app')
@section('content')

<div class="page-title">
    <div class="container">
        <h3 class="text-center">{{ $data->title }}</h3>
    </div>
</div>
<div id="main-wrapper" class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="panel info-box panel-white">
                        <div class="panel-body">
                            <div class="info-box-stats">
                                <p class="counter">{{number_format($data->tot1)}}</p>
                                <span class="info-box-title">Hari Ini</span>
                            </div>
                            <div class="info-box-icon">
                                <i class="icon-calendar"></i>
                            </div>
                            <div class="info-box-progress">
                                <div class="progress progress-xs progress-squared bs-n">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40"
                                        aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel info-box panel-white">
                        <div class="panel-body">
                            <div class="info-box-stats">
                                <p class="counter">{{number_format($data->tot2)}}</p>
                                <span class="info-box-title">Perioritas Tinggi</span>
                            </div>
                            <div class="info-box-icon">
                                <i class="icon-clock"></i>
                            </div>
                            <div class="info-box-progress">
                                <div class="progress progress-xs progress-squared bs-n">
                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80"
                                        aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel info-box panel-white">
                        <div class="panel-body">
                            <div class="info-box-stats">
                                <p><span class="counter">{{number_format($data->tot3)}}</span></p>
                                <span class="info-box-title">Semua</span>
                            </div>
                            <div class="info-box-icon">
                                <i class="icon-drawer"></i>
                            </div>
                            <div class="info-box-progress">
                                <div class="progress progress-xs progress-squared bs-n">
                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60"
                                        aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel info-box panel-white">
                        <div class="panel-body">
                            <div class="info-box-stats">
                                <p class="counter">{{number_format($data->tot4)}}</p>
                                <span class="info-box-title">Dibenderai</span>
                            </div>
                            <div class="info-box-icon">
                                <i class="icon-flag"></i>
                            </div>
                            <div class="info-box-progress">
                                <div class="progress progress-xs progress-squared bs-n">
                                    <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="50"
                                        aria-valuemin="0" aria-valuemax="100" style="width: 50%">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Row -->
        </div>
    </div>

    <h3 class="m-b-sm col-md-10 col-md-offset-1">Daftar Saya</h3>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-white">
                <div class="panel-heading">
                    <h3 class="panel-title">Panel with controls</h3>
                    <div class="panel-control">
                        <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Expand/Collapse"
                            class="panel-collapse"><i class="icon-arrow-down"></i></a>
                        <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Reload"
                            class="panel-reload"><i class="icon-reload"></i></a>
                    </div>
                </div>
                <div class="panel-body">
                    <table class="table table-hover">
                        <tbody>
                            @php
                            $no =1;
                            @endphp
                            @foreach ($data->list_daftar as $ds)
                            <tr>
                                <th scope="row" width="5%">{{$no++}}</th>
                                <td>
                                    <span class="label label-primary" style="padding: 5px"><i
                                            class="{{$ds->icon_daftar}}"></i></span>
                                    {{$ds->nama_daftar}}
                                </td>
                                <td width="20%" class="text-right">
                                    <a href="{{url('/view_todo/daftar_id',$ds->Id)}}"
                                        style="text-decoration: none"><span class="label label-info">View</span></a>
                                    <a href="{{url('/edit_daftar',$ds->Id)}}" style="text-decoration: none"><span
                                            class="label label-primary">Edit</span></a>
                                    <a href="{{url('/save_hapus_daftar',$ds->Id)}}"
                                        onclick="return confirm('Are you sure you want to delete this item')"
                                        style="text-decoration: none"><span class="label label-danger">Hapus</span></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="btn-group btn-group-justified" role="group" aria-label="Justified button group">
                <a href="{{url('/setup_todo')}}" class="btn btn-info" role="button">Pengingat Baru</a>
                <a href="{{url('/setup_daftar')}}" class="btn btn-warning" role="button">Tambah Daftar</a>
            </div>
        </div>
    </div>
</div>

@endsection