@extends('template.app')
@section('content')

<div class="page-title">
    <div class="container">
        <h3 class="text-center">{{ $data->title }}</h3>
    </div>
</div>
<div id="main-wrapper" class="container">

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-white">
                <div class="panel-body">
                    <form class="m-t-md" action="{{ url('save_todo') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <table class="table" style="margin-bottom: 0px">
                            <tbody>
                                <tr>
                                    <td style="vertical-align:middle;">Judul</td>
                                    <td>
                                        <input class="form-control" type="text" id="judul" name="judul"
                                            placeholder="Nama Daftar" value="" autofocus>
                                        @error('judul')
                                        <p class="text-warning">{{ $message }}</p>
                                        @enderror
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align:middle;">Catatan</td>
                                    <td>
                                        <input type="text" class="form-control" id="catatan" name="catatan"
                                            placeholder="Catatan">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align:middle;">Pilih Daftar</td>
                                    <td>
                                        <select name="daftar_id" id="daftar_id" class="form-control">
                                            @foreach ($data->daftar as $ds)
                                            <option value="{{$ds->Id}}">{{$ds->nama_daftar}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align:middle;">Tanggal</td>
                                    <td>
                                        <input type="date" class="form-control" id="tanggal" name="tanggal"
                                            value="{{date('Y-m-d')}}">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align:middle;">Flag</td>
                                    <td>
                                        <select name="flag" id="flag" class="form-control">
                                            <option value="N">No</option>
                                            <option value="Y">Yes</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align:middle;">Prioritas</td>
                                    <td>
                                        <select name="prioritas" id="prioritas" class="form-control">
                                            <option value="0">n/a</option>
                                            <option value="1">Tinggi</option>
                                        </select>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <div style="float:right;">
                            <a href="{{url('/')}}" class="btn btn-warning">Kembali</a>
                            <button type="submit" class="btn btn-success">
                                <i class="glyphicon glyphicon-ok m-r-xs"></i>Simpan Data
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection