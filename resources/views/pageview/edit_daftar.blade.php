@extends('template.app')
@section('content')

<div class="page-title">
    <div class="container">
        <h3 class="text-center">{{ $data->title }}</h3>
    </div>
</div>
<div id="main-wrapper" class="container">

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-white">
                <div class="panel-body">
                    <form class="m-t-md" action="{{ url('save_edit_daftar', $data->data_daftar->Id) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <table class="table" style="margin-bottom: 0px">
                            <tbody>
                                <tr>
                                    <td style="vertical-align:middle;">Nama Daftar</td>
                                    <td>
                                        <input class="form-control" type="text" id="nama_daftar" name="nama_daftar" placeholder="Nama Daftar" value="{{$data->data_daftar->nama_daftar}}" autofocus>
                                        @error('nama_daftar')
                                        <p class="text-warning">{{ $message }}</p>
                                        @enderror
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align:middle;">Icon Daftar</td>
                                    <td>
                                        <input type="text" class="form-control" id="icon_daftar" name="icon_daftar" placeholder="Pilih icon" value="{{$data->data_daftar->icon_daftar}}" readonly>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <div class="line-icons row text-center">
                                            <span class="line-icon-item col-md-1 col-sm-1">
                                                <span class="item">
                                                    <a href="javascript:void(0)" onclick="getIcon(this)" style="text-decoration: none" class="icon-emoticon-smile"></a>
                                                </span>
                                            </span>
                                            <span class="line-icon-item col-md-1 col-sm-1">
                                                <span class="item">
                                                    <a href="javascript:void(0)" onclick="getIcon(this)" style="text-decoration: none" class="icon-fire"></a>
                                                </span>
                                            </span>
                                            <span class="line-icon-item col-md-1 col-sm-1">
                                                <span class="item">
                                                    <a href="javascript:void(0)" onclick="getIcon(this)" style="text-decoration: none" class="icon-badge"></a>
                                                </span>
                                            </span>
                                            <span class="line-icon-item col-md-1 col-sm-1">
                                                <span class="item">
                                                    <a href="javascript:void(0)" onclick="getIcon(this)" style="text-decoration: none" class="icon-calculator"></a>
                                                </span>
                                            </span>
                                            <span class="line-icon-item col-md-1 col-sm-1">
                                                <span class="item">
                                                    <a href="javascript:void(0)" onclick="getIcon(this)" style="text-decoration: none" class="icon-book-open"></a>
                                                </span>
                                            </span>
                                            <span class="line-icon-item col-md-1 col-sm-1">
                                                <span class="item">
                                                    <a href="javascript:void(0)" onclick="getIcon(this)" style="text-decoration: none" class="icon-film"></a>
                                                </span>
                                            </span>
                                            <span class="line-icon-item col-md-1 col-sm-1">
                                                <span class="item">
                                                    <a href="javascript:void(0)" onclick="getIcon(this)" style="text-decoration: none" class="icon-social-dribbble"></a>
                                                </span>
                                            </span>
                                            <span class="line-icon-item col-md-1 col-sm-1">
                                                <span class="item">
                                                    <a href="javascript:void(0)" onclick="getIcon(this)" style="text-decoration: none" class="icon-handbag"></a>
                                                </span>
                                            </span>
                                            <span class="line-icon-item col-md-1 col-sm-1">
                                                <span class="item">
                                                    <a href="javascript:void(0)" onclick="getIcon(this)" style="text-decoration: none" class="icon-list"></a>
                                                </span>
                                            </span>
                                            <span class="line-icon-item col-md-1 col-sm-1">
                                                <span class="item">
                                                    <a href="javascript:void(0)" onclick="getIcon(this)" style="text-decoration: none" class="icon-earphones"></a>
                                                </span>
                                            </span>
                                            <span class="line-icon-item col-md-1 col-sm-1">
                                                <span class="item">
                                                    <a href="javascript:void(0)" onclick="getIcon(this)" style="text-decoration: none" class="icon-key"></a>
                                                </span>
                                            </span>
                                            <span class="line-icon-item col-md-1 col-sm-1">
                                                <span class="item">
                                                    <a href="javascript:void(0)" onclick="getIcon(this)" style="text-decoration: none" class="icon-support"></a>
                                                </span>
                                            </span>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <div style="float:right;">
                            <a href="{{url('/')}}" class="btn btn-warning">Kembali</a>
                            <button type="submit" class="btn btn-success">
                                <i class="glyphicon glyphicon-ok m-r-xs"></i>Simpan Daftar
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function getIcon(obj) {
        var getIcon = $(obj).attr('class')
        $("#icon_daftar").val(getIcon);
    }
</script>

@endsection