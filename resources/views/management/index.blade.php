@extends('template.app')
@section('content')

<div id="main-wrapper" class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-white">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title">WEBSITE PROFILE</h4>
                </div>
                <div class="panel-body">
                    <form class="m-t-md" action="{{ url('app_setting',$data->data_apps->id) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <table class="table table-hover">
                            <tbody>
                                <tr>
                                    <td width="20%" style="vertical-align:middle;">Nama Apps </td>
                                    <td width="80%">
                                        <div class="row">
                                            <div class="col-sm-9">
                                                <input class="form-control" type="text" id="nama_toko" name="nama_toko" placeholder="Nama Profile" value="{{$data->data_apps->nama_apps}}" autofocus>
                                                @error('username')
                                                <p class="text-warning">{{ $message }}</p>
                                                @enderror
                                            </div>
                                            <div class="col-sm-3">
                                                <input class="form-control" type="text" id="alias_toko" name="alias_toko" placeholder="Alias Name" value="{{$data->data_apps->nama_lain}}">
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align:middle;">Alamat Toko</td>
                                    <td>
                                        <input class="form-control" type="text" id="alamat_toko" name="alamat_toko" placeholder="Alamat Profile" value="{{$data->data_apps->alamat}}">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align:middle;">No Telpon</td>
                                    <td>
                                        <input type="text" class="form-control" id="no_telp_toko" name="no_telp_toko" placeholder="No Telepon" value="{{$data->data_apps->no_telp}}">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align:middle;">Alamat Email</td>
                                    <td>
                                        <input type="text" class="form-control" id="email_toko" name="email_toko" placeholder="Alamat Email" value="{{$data->data_apps->email}}">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align:middle;">Header Website</td>
                                    <td>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <img src="{{ asset('assets/img/logoweb/'.$data->data_apps->img_apps) }}" class="img-thumbnail">
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="input-group file-preview" style="padding-right:15px;">
                                                    <input type="text" name="file_name1" id="file_name1" placeholder="logpaminal_new1.png" class="form-control file-preview-filename" disabled="disabled" />
                                                    <!-- don't give a name === doesn't send on POST/GET -->
                                                    <span class="input-group-btn">
                                                        <!-- file-preview-input -->
                                                        <div class="btn btn-default file-preview-input"><span class="glyphicon glyphicon-folder-open"></span> <span class="file-preview-input-title">&nbspBrowse </span>
                                                            <!-- accept="application/pdf" -->
                                                            <input type="file" id="image" name="image" accept="image/*" onchange="document.getElementById('file_name1').value = this.value.split('\\').pop().split('/').pop()" />
                                                            <!-- rename it -->
                                                        </div>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align:middle;">Logo Website</td>
                                    <td>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <img src="{{ asset('assets/img/logoweb/'.$data->data_apps->logo_apps) }}" class="img-thumbnail">
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="input-group file-preview" style="padding-right:15px;">
                                                    <input type="text" name="file_name2" id="file_name2" placeholder="logpaminal_new.png" class="form-control file-preview-filename" disabled="disabled" />
                                                    <!-- don't give a name === doesn't send on POST/GET -->
                                                    <span class="input-group-btn">
                                                        <!-- file-preview-input -->
                                                        <div class="btn btn-default file-preview-input"><span class="glyphicon glyphicon-folder-open"></span> <span class="file-preview-input-title">&nbspBrowse </span>
                                                            <!-- accept="application/pdf" -->
                                                            <input type="file" id="image2" name="image2" accept="image/*" onchange="document.getElementById('file_name2').value = this.value.split('\\').pop().split('/').pop()" />
                                                            <!-- rename it -->
                                                        </div>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div style="float:right;">
                            <button type="submit" class="btn btn-success">
                                <i class="glyphicon glyphicon-ok m-r-xs"></i>Update
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div><!-- Row -->

@endsection