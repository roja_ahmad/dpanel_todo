<!DOCTYPE html>
<html>

<head>
    <link rel="icon" href="{{ asset('assets/img/logoweb/'.$data->data_apps->logo_apps) }}" type="image/x-icon">
    <!-- Title -->
    <title>{{$data->data_apps->nama_lain}} | {{$data->title}}</title>

    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta charset="UTF-8">
    <meta name="description" content="Admin Dashboard Template" />
    <meta name="keywords" content="admin,dashboard" />
    <meta name="author" content="Steelcoders" />

    <!-- Styles -->
    <link href='http://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700' rel='stylesheet' type='text/css'>
    <link href="{{ asset('assets/plugins/pace-master/themes/blue/pace-theme-flash.css')}}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/uniform/css/uniform.default.min.css')}}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/fontawesome/css/font-awesome.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/line-icons/simple-line-icons.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/waves/waves.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/switchery/switchery.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/3d-bold-navigation/css/style.css')}}" rel="stylesheet" type="text/css" />

    <!-- Theme Styles -->
    <link href="{{ asset('assets/css/modern.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/custom.css')}}" rel="stylesheet" type="text/css" />

    <script src="{{ asset('assets/plugins/3d-bold-navigation/js/modernizr.js')}}"></script>

    <link rel="stylesheet" href="{{ asset('assets/noty/noty.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('assets/noty/themes/bootstrap-v3.css') }}" type="text/css" />

</head>

<body class="page-login">
    <main class="page-content">
        <div class="page-inner">
            <div id="main-wrapper">
                <div class="row">
                    <div class="col-md-3 center" style="padding-top:90px;">
                        <div class="login-box">
                            <a href="index.html" class="logo-name text-lg text-center">{{$data->data_apps->nama_lain}}</a>
                            <p class="text-center m-t-md">Please login into your account.</p>
                            <form class="m-t-md" action="{{ url('login') }}" method="POST">
                                @csrf
                                <div class="form-group">
                                    <input type="text" class="form-control" name="username" placeholder="Username" value="{{ old('username') }}" autofocus>
                                    @error('username')
                                    <p class="text-warning">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" name="password" placeholder="Password">
                                    @error('password')
                                    <p class="text-warning">{{ $message }}</p>
                                    @enderror
                                </div>
                                <button type="submit" class="btn btn-success btn-block">Login</button>
                            </form>
                            <p class="text-center m-t-xs text-sm">{{ date('Y') }} &copy;
                                {{$data->data_apps->nama_apps.$data->data_apps->version }} by Ahmad Roza.</p>
                        </div>
                    </div>
                </div><!-- Row -->
            </div><!-- Main Wrapper -->
        </div><!-- Page Inner -->
    </main><!-- Page Content -->


    <!-- Javascripts -->
    <script src="{{ asset('assets/plugins/jquery/jquery-2.1.4.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/pace-master/pace.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/jquery-blockui/jquery.blockui.js')}}"></script>
    <script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/switchery/switchery.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/uniform/jquery.uniform.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/classie/classie.js')}}"></script>
    <script src="{{ asset('assets/plugins/waves/waves.min.js')}}"></script>
    <script src="{{ asset('assets/js/modern.min.js')}}"></script>

    <script src="{{ asset('assets/noty/noty.min.js') }}"></script>

    <script type="text/javascript">
        Noty.overrideDefaults({
            layout: 'topRight',
            theme: 'bootstrap-v3',
            timeout: 4000
        });
    </script>

    @if(Session::has('success'))
    <script type="text/javascript">
        new Noty({
            text: '<strong>Success</strong> {{Session::get("success")}}',
            layout: 'topRight',
            type: 'success',
        }).setTimeout(4000).show();
    </script>
    @endif
    @if(Session::has('fail'))
    <script type="text/javascript">
        new Noty({
            text: '<strong>Fails</strong> {{Session::get("fail")}}',
            layout: 'topRight',
            type: 'warning'
        }).setTimeout(4000).show();
    </script>
    @endif
</body>

</html>