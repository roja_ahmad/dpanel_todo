<!DOCTYPE html>
<html>

<head>
    <link rel="icon" href="{{ asset('assets/img/logoweb/'.$data->data_apps->logo_apps) }}" type="image/x-icon">
    <!-- Title -->
    <title>{{$data->data_apps->nama_lain}} | {{$data->title}}</title>

    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta charset="UTF-8">
    <meta name="description" content="Admin Dashboard Template" />
    <meta name="keywords" content="admin,dashboard" />
    <meta name="author" content="Steelcoders" />

    <!-- Styles -->
    <link href='http://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700' rel='stylesheet' type='text/css'>
    <link href="{{ asset('assets/plugins/pace-master/themes/blue/pace-theme-flash.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/uniform/css/uniform.default.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/fontawesome/css/font-awesome.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/line-icons/simple-line-icons.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/waves/waves.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/switchery/switchery.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/3d-bold-navigation/css/style.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/slidepushmenus/css/component.css') }}" rel="stylesheet" type="text/css" />

    <!-- Theme Styles -->
    <link href="{{ asset('assets/css/modern.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
    <script src="{{ asset('assets/plugins/3d-bold-navigation/js/modernizr.js') }}"></script>

    <!-- Noty Styles -->
    <link rel="stylesheet" href="{{ asset('assets/noty/noty.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('assets/noty/themes/bootstrap-v3.css') }}" type="text/css" />

    <style>
        .file-preview-input {
            position: relative;
            overflow: hidden;
            margin: 0px;
            color: #333;
            /* background-color: #fff; */
            border-color: #ccc;
        }

        .file-preview-input input[type=file] {
            position: absolute;
            top: 0;
            right: 0;
            margin: 0;
            padding: 0;
            font-size: 20px;
            cursor: pointer;
            opacity: 0;
            filter: alpha(opacity=0);
        }
    </style>
</head>

<body class="page-header-fixed compact-menu page-horizontal-bar">
    <div class="overlay"></div>
    <main class="page-content content-wrap">
        @include('template.topmenu')
        @include('template.menubar')
        <div class="page-inner">
            @yield('content')
            @include('template.footer')
        </div><!-- Page Inner -->
    </main><!-- Page Content -->
    <div class="cd-overlay"></div>

    <!-- Javascripts -->
    <script src="{{ asset('assets/plugins/jquery/jquery-2.1.4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/pace-master/pace.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/jquery-blockui/jquery.blockui.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/switchery/switchery.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/uniform/jquery.uniform.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/classie/classie.js') }}"></script>
    <script src="{{ asset('assets/plugins/waves/waves.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/3d-bold-navigation/js/main.js') }}"></script>
    <script src="{{ asset('assets/js/modern.min.js') }}"></script>

    <script src="{{ asset('assets/noty/noty.min.js') }}"></script>

    <script type="text/javascript">
        Noty.overrideDefaults({
            layout: 'topRight',
            theme: 'bootstrap-v3',
            timeout: 4000
        });
    </script>

    @if(Session::has('success'))
    <script type="text/javascript">
        new Noty({
            text: '<strong>Success</strong> {{Session::get("success")}}',
            layout: 'topRight',
            type: 'success',
        }).setTimeout(4000).show();
    </script>
    @endif
    @if(Session::has('fail'))
    <script type="text/javascript">
        new Noty({
            text: '<strong>Fails</strong> {{Session::get("fail")}}',
            layout: 'topRight',
            type: 'warning'
        }).setTimeout(4000).show();
    </script>
    @endif

</body>

</html>