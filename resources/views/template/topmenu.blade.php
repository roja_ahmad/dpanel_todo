<div class="navbar">
    <div class="navbar-inner container">
        <div class="sidebar-pusher">
            <a href="javascript:void(0);" class="waves-effect waves-button waves-classic push-sidebar">
                <i class="fa fa-bars"></i>
            </a>
        </div>
        <div class="logo-box">
            <a href="{{url('/')}}" class="logo-text"><span>{{$data->data_apps->nama_lain}}</span></a>
        </div><!-- Logo Box -->
        {{-- <div class="search-button">
            <a href="javascript:void(0);" class="waves-effect waves-button waves-classic show-search"><i
                    class="fa fa-search"></i></a>
        </div> --}}
        <div class="topmenu-outer">
            <div class="top-menu">
                <ul class="nav navbar-nav navbar-left">
                    <li>
                        <a href="javascript:void(0);" class="waves-effect waves-button waves-classic sidebar-toggle"><i class="fa fa-bars"></i></a>
                    </li>
                    <li>
                        <a href="{{url('/')}}" class="waves-effect waves-button waves-classic"><i class="fa fa-home"></i></a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="waves-effect waves-button waves-classic toggle-fullscreen"><i class="fa fa-expand"></i></a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle waves-effect waves-button waves-classic" data-toggle="dropdown">
                            <span class="user-name">{{ $data->data_user->name }}<i class="fa fa-angle-down"></i></span>
                            <img class="img-circle avatar" src="{{ asset('assets/img/profile/default.jpg') }}" width="40" height="40" alt="">
                        </a>
                        <ul class="dropdown-menu dropdown-list" role="menu">
                            <li role="presentation"><a href="{{url('/profile')}}"><i class="fa fa-user"></i>Profile</a>
                            </li>
                            <li role="presentation"><a href="{{url('/app_setting')}}"><i class="fa fa-cog"></i>App
                                    Setting</a></li>
                            <li role="presentation" class="divider"></li>
                            <li role="presentation"><a href="{{url('/logout')}}"><i class="fa fa-sign-out m-r-xs"></i>Log out</a></li>
                        </ul>
                    </li>
                </ul><!-- Nav -->
            </div><!-- Top Menu -->
        </div>
    </div>
</div><!-- Navbar -->